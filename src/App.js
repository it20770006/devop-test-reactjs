
import './App.css';
import Sidebar from './components/sidebar/Sidebar';
import Home from './components/home/Home';
import About from './components/about/About';
import Service from './components/Services/Services';
import Resume from './components/resume/Resume';
import Protfolio from './components/protfolio/Protfolio';
import Testimonials from './components/testimonials/Testimonials';
import Contact from './components/contact/Contact';

function App() {
  return (
    <>
    <Sidebar />
    <main className='main'>
        <Home />
        <About />
        <Service />
        <Resume />
        <Protfolio />
        <Testimonials />
        <Contact />
    </main>
     </>
    
  );
}

export default App;
