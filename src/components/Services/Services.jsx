import './services.css';
import Image1 from "../../assets/service-1.svg";
import Image2 from "../../assets/service-2.svg";
import Image3 from "../../assets/service-3.svg";

const data = [
   { 
    id:1,
    image:Image1,
    title:"UI/UX design",
    description:"I will create a quality user interface for your iOS or Android application.I will create a distinctive, expert, contemporary, and clean user interface for your application.I have over three years of professional experience as a graphic and UI designer, and I have worked with hundreds of clients who are happy with my work. ",
   },

   { 
    id:2,
    image:Image2,
    title:"Web Development",
    description:"You're in the perfect place if you want a beautiful React js web application.I'm a skilled front end web developer who has completed numerous projects to the complete delight of my clients.I am proficient in React js front end web development.My objective is to assist businesses in creating an online presence through websites and e-commerce platforms.   ",
   },

   { 
    id:3,
    image:Image3,
    title:"Graphics & Design",
    description:"If you are looking for modern, unique, professional, custom, minimalist, 3d business logo design, then you are at right place. You will get excellent brand identity design service at an affordable price. Quality and Clients' satisfaction get the topmost priority in delivering designs."
   },

];

const Services = () => {
    return ( 
        <section className="container section" id="services">
            <h2 className="section__title">Services</h2>
            <div className="services__container grid">
                {data.map(({id,image,title,description})=>{
                    return(
                        <div className="services__card" key={id}>
                            <img src={image} alt="" className="services__img" />
                            <h3 className="services__title">{title}</h3>
                            <p className="services__description">{description}</p>
                        </div>
                    )
                })}
            </div>
        </section>
     );
}
 
export default Services;