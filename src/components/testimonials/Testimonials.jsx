import "./testimonials.css";
import Image1 from "../../assets/client-1.jpg";
import Image3 from "../../assets/client2.jpg";

// import Swiper core and required modules
import { Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

const data = [
  {
    id: 1,
    image: Image1,
    title: "Chathuri Maduka",
    subtitle: "Workhub24-QA Intern",
    comment:
      "Nethma is a very talented software developer.He is an allrounder in his industry and he is an experts in web development. He has always given best support to us and he has good understanding about the clients requirements. He will finish his work according to the time frame. Nethma is an useful resource for your company.",
  },
  {
    id: 2,
    image: Image3,
    title: "Malith Madushan",
    subtitle: "Web Application Developer at Online Software Solution",
    comment:
      "Nethma is an expert in PHP, javascript, java. Nethma has a fast learner and has deep knowledge of PHP.He is a hard worker talented young boy and a dedicated person who will complete your project in a given time frame.",
  },
];

const Tetimonials = () => {
  return (
    <section className="testimonials container section">
      <h2 className="section__title">Client & Reviews</h2>
      <Swiper
        className="testimonials__container grid"
        // install Swiper modules
        modules={[Pagination]}
        spaceBetween={30}
        slidesPerView={1}
        loop={true}
        grabCursor={true}
        pagination={{ clickable: true }}
      >
        {data.map(({ id, image, title, subtitle, comment }) => {
          return (
            <SwiperSlide className="testimonial__item" key={id}>
              <div className="thumb">
                <img src={image} alt="" />
              </div>
              <h3 className="testimonials__title">{title}</h3>
              <span className="subtitle">{subtitle}</span>
              <div className="comment">{comment}</div>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </section>
  );
};

export default Tetimonials;
