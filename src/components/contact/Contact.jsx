import "./contact.css";
import React, { useRef } from "react";
import emailjs from "@emailjs/browser";
const Contact = () => {
  // Email details
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_g051tdg",
        "template_f9hjqvn",
        form.current,
        "v_xxuC6-pWe1uesSj"
      )
      .then(
        (result) => {
          console.log(result.text);
          console.log("Message sent")
          alert("Message Sent")
        },
        (error) => {
          console.log(error.text);
        }
      );
  };
  return (
    <section className="contact container section" id="contact">
      <h2 className="section__title">Get In Touch</h2>

      <div className="contact__container grid">
        <div className="contact__info">
          <h3 className="contact__title">Let's talk about everything!</h3>
          <p className="contact__details">Send Me a Email💗</p>
        </div>

        <form ref={form} onSubmit={sendEmail} className="contact__form">
          <div className="contact__form-group">
            <div className="contact__form-div">
              <input
                type="text"
                name="user_name"
                className="contact__form-input"
                placeholder="Insert Your Name"
              />
            </div>

            {/* 2input */}
            <div className="contact__form-div">
              <input
                type="email"
                name="user_email"
                className="contact__form-input"
                placeholder="Insert Your Email"
              />
            </div>
          </div>
          {/* 3input */}
          <div className="contact__form-div">
            <input
              type="text"
              name="from_name"
              className="contact__form-input"
              placeholder="Insert Your Subject"
            />
          </div>

          <div className="contact__form-div contact__form-area">
            <textarea
              name="message"
              id=""
              cols=""
              rows="10"
              className="contact__form-input"
              placeholder="write Your Message"
            />
          </div>
          <button className="btn" value="Send">
            Send Message
          </button>
        </form>
      </div>
    </section>
  );
};

export default Contact;
