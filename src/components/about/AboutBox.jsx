import React from "react";

export const AboutBox = () => {
  return (
    <div className="about__boxes grid">
      <div className="about__box">
        <i className="about__icon icon-fire"></i>
        <div>
          <h3 className="about__titles">10</h3>
          <span className="about__subtitle">Project Completed</span>
        </div>
      </div>

      {/* 2 */}
      <div className="about__box">
      <i class="about__icon icon-home"></i>
        <div>
          <h3 className="about__titles">10</h3>
          <span className="about__subtitle">Satisfied Clients</span>
        </div>
      </div>

      {/* 3 */}

      <div className="about__box">
      <i class="about__icon icon-people"></i>
        <div>
          <h3 className="about__titles">3</h3>
          <span className="about__subtitle">Companies</span>
        </div>
      </div>

      {/* 4 */}
      <div className="about__box">
      <i class="about__icon icon-calendar"></i>
        <div>
          <h3 className="about__titles">1</h3>
          <span className="about__subtitle">Working Expiriens</span>
        </div>
      </div>
    </div>
  );
};
