import "./about.css";
import Apic from "../../assets/about.png";
import { AboutBox } from "./AboutBox";
const About = () => {

  // PDF Download 
  const onButtonClick = ()=>{
    fetch('nethmaDissanayake.pdf').then(response =>{
      response.blob().then(blob =>{
        const fileURL = window.URL.createObjectURL(blob);
        let alink = document.createElement('a');
        alink.href = fileURL;
        alink.download = 'nethmaDissanayake.pdf';
        alink.click();
      })
    })
  }

  
  return (
    <section className="about container section" id="a">
      <h2 className="section__title">About Me</h2>
      <div className="about__container grid">
        <img src={Apic} alt="" className="about__img" />

        <div className="about__data grid">
          <div className="about__info">
            <p className="about__description">
              I am an outgoing individual who is eager to explore and learn new
              things. As a fast learner, I bring energy and enthusiasm to the
              team, along with great interpersonal and leadership skills.
            </p>
            <a href="#d" className="btn" onClick={onButtonClick}>
              Download CV
            </a>
          </div>
          <div className="about__skills grid">
            <div className="skills__data">
              <div className="skills__titles">
                <h3 className="skills__name ">Development</h3>
                <span className="skills__number ">70%</span>
              </div>

              <div className="skills__bar">
                <spam className="skills__percentage development"></spam>
              </div>
            </div>

            <div className="skills__data">
              <div className="skills__titles">
                <h3 className="skills__name ">U/UX Design</h3>
                <span className="skills__number ">80%</span>
              </div>

              <div className="skills__bar">
                <spam className="skills__percentage design"></spam>
              </div>
            </div>

            <div className="skills__data">
              <div className="skills__titles">
                <h3 className="skills__name ">Freelancer</h3>
                <span className="skills__number ">40%</span>
              </div>

              <div className="skills__bar">
                <spam className="skills__percentage free"></spam>
              </div>
            </div>

            
          </div>
        </div>
      </div>
      <AboutBox />
    </section>
  );
};

export default About;
