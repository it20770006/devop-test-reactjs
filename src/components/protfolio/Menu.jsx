import Work1 from "../../assets/1.jpg";
import Work2 from "../../assets/caddazdev.PNG";
import Work3 from "../../assets/work-3.svg";
import Work4 from "../../assets/face-cover.PNG";
import Work5 from "../../assets/work-5.svg";
import Work6 from "../../assets/work-6.svg";

const Menu = [
    {
      id: 1,
      image: Work1,
      title: "E-hungry Food Management System ",
      category: "Design",
      url:"https://www.behance.net/gallery/149638055/E-Hungry",
    },
    
    {
      id: 2,
      image: Work2,
      title: "CDAZZDEV Web page Re-design",
      category: "Design",
      url:"https://www.behance.net/gallery/149631155/Software-Company",
    },
    {
      id: 3,
      image: Work3,
      title: "Delivery App Wireframe",
      category: "Branding",
    },
    
    {
      id: 4,
      image: Work4,
      title: "Employees Management System",
      category: "Design",
      url:"https://www.behance.net/gallery/154492307/Employee-management-System"
    },
    
    {
      id: 5,
      image: Work5,
      title: "iMac Mockup Design",
      category: "Creative",
    },
    
    {
      id: 6,
      image: Work6,
      title: "Game Store App Concept",
      category: "Art",
    },
  ];

export default Menu;


