import './home.css';
import Me from "../../assets/me.jpg";
import HeaderSocials from './HeaderSocials';
import ScrollDown from './ScrollDown';
import Shapes from './Shapes';
const Home = () => {
    return ( 
      <section className="home" id="home">
        <div className="intro">
            <img src={Me} alt="" className="home__img" />
            <div className="home__name">
                <h1 className="home__name">Nethma Dissanayake</h1>
                <span className="home__education">
                    I am Front-End developer
                </span>
                <HeaderSocials />
                <a href="#work" className="btn"># Hire Me</a>
                <ScrollDown />
            </div>
            
        </div>
        <Shapes/>
      </section>
     );
}
 
export default Home;