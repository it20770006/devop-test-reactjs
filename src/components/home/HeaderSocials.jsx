import React from "react";


const HeaderSocials = () => {
    return ( 
        <div className="home__socials">
            <a href="https://www.instagram.com/neth_dissa_99/" className="home__social-link" target="_blank">
               <i className="fa-brands fa-instagram"></i>
            </a>

            
            <a href="https://www.behance.net/nethmadisanay" className="home__social-link" target="_blank">
                <i className="fa-brands fa-behance"></i>
            </a>

            <a href="https://www.linkedin.com/in/nethma-dissanayake-1696a5194/" className="home__social-link" target="_blank">
            <i class="fa-brands fa-linkedin"></i>
            </a>

            <a href="https://www.youtube.com/channel/UC9Ty2E9USaPGWtGzNHCO9Ag" className="home__social-link" target="_blank">
            <i class="fa-brands fa-youtube"></i>
            </a>
            
        </div>
     );
}
 
export default HeaderSocials;