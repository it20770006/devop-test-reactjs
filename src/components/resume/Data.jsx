

    const Data = [
        {
          id: 1,
          category: "Education",
          icon: "icon-graduation",
          year: "2020 - 2023",
          title: "Computer Science BSc (Hons) At SLIIT",
          desc: "I graduated with a degree in University of Bedfordshire",
        },
        {
          id: 2,
          category: "experience",
          icon: "icon-briefcase",
          title: "CDAZZDEV",
          year: "2022-2022",
          desc: "I joined this company as a back-end and front-end web developer",
        },
        {
          id: 3,
          category: "experience",
          icon: "icon-briefcase",
          title: "Ebranding Inovetions",
          year: "2021 - 2022",
          desc: "I joined this company as a back-end Developer.",
        },
        {
          id: 4,
          category: "experience",
          icon: "icon-briefcase",
          title: "CodeScale",
          year: "2021 - 2022",
          desc: "I joined this company as a Front-End Developer",
        },
        
      
      ];
      
export default Data;
